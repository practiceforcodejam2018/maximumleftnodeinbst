#include <iostream>
#include <vector>

using namespace std;


typedef struct node_structure
{
    int                   data;
    struct node_structure *left;
    struct node_structure *right;
}node_struct;

class BinaryTree
{
    private:
        node_struct* createNewNodeWithData(int data)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data  = data;
                newNode->left  = NULL;
                newNode->right = NULL;
            }
            return newNode;
        }
    
    public:
        node_struct* createBinaryTreeFromVector(vector<int> nodesDataVector)
        {
            int                   nodesDataVectorSize = (int)(nodesDataVector.size());
            vector<node_struct *> nodesVector;
            node_struct           *headNode           = NULL;
            int                   currentVecSizeIndex = 0;
            int                   nodeIndex           = -1;
            while (currentVecSizeIndex < nodesDataVectorSize-1)
            {
                if (!headNode)
                {
                    headNode   = createNewNodeWithData(nodesDataVector[0]);
                    nodesVector.push_back(headNode);
                }
                nodesVector[++nodeIndex]->left = createNewNodeWithData(nodesDataVector[++currentVecSizeIndex]);
                nodesVector[nodeIndex]->right  = createNewNodeWithData(nodesDataVector[++currentVecSizeIndex]);
                nodesVector.push_back(nodesVector[nodeIndex]->left);
                nodesVector.push_back(nodesVector[nodeIndex]->right);
            }
            return headNode;
        }
    
        void traverseAndDisplayBinaryTreeWithHeadNodeInInInorder(node_struct *head)
        {
            if (!head)
            {
                return;
            }
            traverseAndDisplayBinaryTreeWithHeadNodeInInInorder(head->left);
            cout<<head->data<<" ";
            traverseAndDisplayBinaryTreeWithHeadNodeInInInorder(head->right);
        }
};

class Engine
{
    private:
        int max = INT_MIN;
    
    public:
        int findMaximumLeftNodeElementInTreeWithHeadNode(node_struct *head)
        {
            if (!head)
            {
                return max;
            }
            findMaximumLeftNodeElementInTreeWithHeadNode(head->left);
            if (head->left && head->left->data > max)
            {
                max = head->left->data;
            }
            findMaximumLeftNodeElementInTreeWithHeadNode(head->right);
            return max;
        }
};


int main(int argc, const char * argv[])
{
    vector<int> nodesDataVector = {5 , 6 , 3 , 4 , 9 , 10 , 8 , 1 , 15 , 2 , 16 , 2 , 25 , -3 , 40};
    BinaryTree  bTree           = BinaryTree();
    node_struct *head           = bTree.createBinaryTreeFromVector(nodesDataVector);
    bTree.traverseAndDisplayBinaryTreeWithHeadNodeInInInorder(head);
    cout<<endl<<endl;
    Engine e                    = Engine();
    cout<<e.findMaximumLeftNodeElementInTreeWithHeadNode(head)<<endl;
    return 0;
}
